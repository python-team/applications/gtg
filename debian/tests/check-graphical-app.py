#! /bin/python3

import os
import signal
import sys

from dogtail.config import config
config.logDebugToFile = False
from dogtail.procedural import *
from dogtail.tc import TCNode, TCBool
tcn = TCNode()

pid = run("gtg")
tcn.compare("app exists", None, focus.application.node)

app_name = "Getting Things GNOME!"
focus.widget.node = focus.app.node.child(name=app_name)
tcn.compare("app has a frame named %s" % app_name, None, focus.widget.node)
os.kill(pid, signal.SIGTERM)

